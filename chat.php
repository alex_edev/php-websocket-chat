<?php
/**
 * Setting up chat constants
 */
namespace Config\Chat;

// Max number of rows to be read per one request from chat history table 
define(__NAMESPACE__.'\MSG_READ_LIMIT', 50);
// Max number of message to be stored in cache per chat
define(__NAMESPACE__.'\MSG_CACHE_LIMIT', 100);
// Period of time (seconds) since last user activity when he is treated as active in chat
define(__NAMESPACE__.'\USER_ACTIVE_TIMEOUT', 300);
// Time for keeping chat entry in cache after which this entry must be treated as expired and be erased
define(__NAMESPACE__.'\EXPIRATION_TIME', 86400);
