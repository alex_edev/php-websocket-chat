#!/usr/bin/php
<?php
use Hoa\Websocket\{
    Server,
    Connection
};
use Hoa\Event\Bucket;
use AlexEdev\Chat\{
    Handler,
    DataSet,
    EdevChatException
};
use Models\{
    UserModel, 
    UserModelException, 
    UserModelDBException
};

// Set up autoloaders
require_once __DIR__.'/vendor/autoload.php';
// Set up error handlers
require_once __DIR__.'/config/errors.php';
// Set up chat constants
require_once __DIR__.'/config/chat.php';
// Set up policy rules
require_once __DIR__.'/config/policy.php';
// Get database handler
$db = require __DIR__.'/config/db.php';
// Get cache handler
$mc = require __DIR__.'/config/memcached.php';
// Get chat handler
$handler = new Handler($db, $mc, function(array $authCredentials): array
{
    $authKey = $authCredentials['authKey'] ?? '';
    $login = $authCredentials['login'] ?? '';
    $password = $authCredentials['password'] ?? '';
    $sessId = $authCredentials['sessId'] ?? '';

    if (empty($authKey) && empty($login) && empty($password)) {
        return array(
            'result' => false,
            'reason' => ''
        );
    } else {
        // Check user authorization
        try {
            if (!empty($login) && !empty($password)) {
                // Try to authorize by login/password
                $user = new UserModel($this->db, $login, $password);
            } else {
                // Try to authorize by key
                $user = new UserModel($this->db, $authKey);
            }

            /* 
            * Update user session id if it has changed (usefull in situations when user sends requests 
            * with authorization by login/password pair and from different browsers)
            */
            if (!empty($sessId) && $user->lastSessId() !== $sessId) {
                $user->lastSessId($sessId);
                $user->updateUser();
            }

            // Check user access rights
            if (!in_array('chat', $user->access())) {
                return array(
                    'result' => false,
                    'reason' => 'Access denied for this account.'
                );
            }
            
            return array(
                'result' => true,
                'login' => $user->login(),
                'key' => $user->authKey()
            );
        } catch (UserModelException $e) {
            // Throw an error to log exception without stopping the script execution
            trigger_error($e->getMessage()." in ".$e->getFile()." on line ".$e->getLine(), E_USER_WARNING);
            
            if ($e instanceof UserModelDBException) {
                // Exception from the database stops script execution
                exit;
            } else {
                return array(
                    'result' => false,
                    'reason' => 'Wrong login and/or password.'
                );
            }
        }
    }
});

// Init websocket server
$webSocket = new Hoa\Websocket\Server(
    new Hoa\Socket\Server('ws://127.0.0.1:8889')
);

// Assign event handlers
$webSocket->on('message', function (Hoa\Event\Bucket $bucket) {
    $rest = array('meta' => array('code' => 200), 'data' => array());
    $request = json_decode($bucket->getData()['message'], true);
    $currentConnectionId = $bucket->getSource()->getConnection()->getCurrentNode()->getId();
    $connections = $bucket->getSource()->getConnection()->getNodes();

    // Handle message
    try {
        $dataSet = new DataSet($currentConnectionId, $request);
        $GLOBALS['handler']->handle($dataSet);
        
        foreach ($dataSet as $connectionId => $data) {
            $rest['data'] = $data;

            if (array_key_exists($connectionId, $connections)) {
                try {
                    // Send to current connection
                    $bucket->getSource()->send(json_encode($rest), $connections[$connectionId]);
                } catch (\Exception $e) {
                    // Throw an error to log exception without stopping the script execution
                    trigger_error($e->getMessage()." in ".$e->getFile()." on line ".$e->getLine(), 
                        E_USER_WARNING);
                }
            }
        }
    } catch (EdevChatException $e) {
        $rest['data'] = array('requestId' => $request['requestId']);
        $rest['meta']['code'] = 400;
        $rest['meta']['errorType'] = 'EdevChatException';
        $rest['meta']['errorMessage'] = $e->getMessage();

        // Throw an error to log exception without stopping the script execution
        trigger_error($e->getMessage()." in ".$e->getFile()." on line ".$e->getLine(), E_USER_WARNING);
        // Send response to client
        $bucket->getSource()->send(json_encode($rest));
    }
});
$webSocket->on('close', function (Hoa\Event\Bucket $bucket) {
    $data = $bucket->getData();

    if ($data['code'] !== Hoa\Websocket\Connection::CLOSE_NORMAL &&
        $data['code'] !== Hoa\Websocket\Connection::CLOSE_GOING_AWAY) {
        // Throw an error to log abnormal connection closing 
        trigger_error("Chat server abnormal connection closing code ".$data['code'], E_USER_WARNING);
    }

    $rest = array('meta' => array('code' => 200), 'data' => array());
    $request = array('command' => 'disconnect');
    $currentConnectionId = $bucket->getSource()->getConnection()->getCurrentNode()->getId();
    $connections = $bucket->getSource()->getConnection()->getNodes();

    // Handle connection closing
    try {
        $dataSet = new DataSet($currentConnectionId, $request);
        $GLOBALS['handler']->handle($dataSet);
        
        foreach ($dataSet as $connectionId => $data) {
            $rest['data'] = $data;

            if (array_key_exists($connectionId, $connections)) {
                try {
                    // Send to current connection
                    $bucket->getSource()->send(json_encode($rest), $connections[$connectionId]);
                } catch (\Exception $e) {
                    /* 
                     * MAYBE BUG: constant throwing of \Hoa\Stream\Exception in _setStream() method of
                     * hoa/stream/Stream.php during handling connection closing looks like a bug because
                     * it is definitely been thrown on an opened connection. 
                     * Check changes with it in future versions of Hoa\Websocket.
                     */
                    if (get_class($e) != 'Hoa\Stream\Exception' || $e->getCode() !== 1) {
                        // Throw an error to log exception without stopping the script execution
                        trigger_error($e->getMessage()." in ".$e->getFile()." on line ".$e->getLine(), 
                            E_USER_WARNING);
                    }
                }
            }
        }
    } catch (EdevChatException $e) {
        // Throw an error to log exception without stopping the script execution
        trigger_error($e->getMessage()." in ".$e->getFile()." on line ".$e->getLine(), E_USER_WARNING);
    }
});
$webSocket->on('error', function (Hoa\Event\Bucket $bucket) {
    // Throw an error to log exception without stopping the script execution
    trigger_error($bucket->getData()['exception']." in websocket.php.", E_USER_WARNING);
});

$webSocket->run();