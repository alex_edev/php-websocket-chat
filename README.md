# php-websocket-chat

This is a Php version of the chat implemented on edev-service.ru

## Usage

This code is represented as is (without any detailed how-to's) but 
it comes with UML diagrams which may help anyone who is interested in using 
this code totally or partially in their own projects.

### Files description

**wschat.php** - this is a chat executable file. Start your study of this project from here.

**chat.php** - config constants.

**Chat/** - chat classes.

**Diagrams/** - UML diagrams drawed in https://draw.io online editor.  

### Dependencies

Here are the short descriptions of dependencies used in this project.
They must be resolved according to your project's directories structure.

**Hoa\Websocket (\^3.17)** - https://packagist.org/packages/hoa/websocket

**Hoa\Event\Bucket (~1.0)** - https://packagist.org/packages/hoa/event

**UserModel**, **UserModelException**, **UserModelDBException** - these are the 
implementations of user authorization/registration scripts. They are not 
published here for security reasons. You need to provide your own if you 
want to use this chat on your site.

### Tests

Tests are not implemented but you are welcome to write them by yourself if you need.