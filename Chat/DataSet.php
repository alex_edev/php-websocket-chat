<?php
namespace AlexEdev\Chat;

use AlexEdev\Chat\Messages\Message;
use const Config\Policy\{
    ALLOWED_SYMBOLS,
    PROHIBITED_WORDS
};

/**
 * Request/response data container
 */
class DataSet implements \Iterator
{
    /** @var string Id of the connection this dataset is obtained from */
    private $connectionId;

    /** @var array Request array */
    private $request;

    /** @var array Response array where $key is a connection id, $value is a response data array */
    private $response = array();

    /** @var int Cursor for itetator realization */
    private $cursor = 0;
    
    public function __construct(string $connectionId, array $request)
    {
        $this->connectionId = $connectionId;
        $this->request = $this->validate($request);
        $this->response[$this->connectionId] = array();

        if (isset($this->request['requestId'])) {
            $this->response[$this->connectionId]['requestId'] = $this->request['requestId'];
        }
    }


    /**
     * Validate request
     * 
     * @param array $request Client request
     * @return array Validated request array
     */
    private function validate(array $request): array 
    {
        $requiredStructure = array(
            'command' => 'string',
        );

        $optionalStructure = array(
            'requestId' => 'integer',
            'chatId' => 'string',
            'message' => 'string',
            'with' => 'string',
            'credentials' => 'array'
        );

        foreach ($requiredStructure as $key => $value) {
            if (gettype($request[$key]) !== $value) {
                throw new EdevChatException("Request parameter '$key' is of incorrect type ".
                    gettype($request[$key]), 0);
            }
        }

        foreach ($optionalStructure as $key => $value) {
            if (isset($request[$key]) && gettype($request[$key]) !== $value) {
                throw new EdevChatException("Request parameter '$key' is of incorrect type ".
                    gettype($request[$key]), 0);
            }
        }

        if (!empty($request['message'])) {
            if (!$this->checkSymbols($request['message'])) {
                throw new EdevChatException('Message contains characters not from ['.ALLOWED_SYMBOLS.']', 0);
            }

            if (!$this->checkPolicy($request['message'])) {
                throw new EdevChatException('Message contains non-normative lexic', 0);
            }
        }

        return $request;
    }


    /**
     * Verifies content against policy rules
     * 
     * @param string $content String to check
     * @return bool TRUE if check successfully passed, FALSE otherwise
     */
    private function checkPolicy(string $content): bool
    {
        if (preg_match('/'.PROHIBITED_WORDS.'/i', $content)) { // check for prohibited words
            return false;
        }

        return true;  
    }


    /**
     * Verifies content against symbols restrictions
     * 
     * @param string $content String to check
     * @return bool TRUE if check successfully passed, FALSE otherwise
     */
    private function checkSymbols(string $content): bool
    {   
        if (!preg_match('/^['.ALLOWED_SYMBOLS.']+$/', $content)) { // check for prohibited symbols
            return false;
        }

        return true;  
    }


    /**
     * Get id of a connection origin of current request
     * 
     * @return string Connection id
     */
    public function getConnectionId(): string
    {
        return $this->connectionId;
    }


    /**
     * Get request command
     * 
     * @return string Command name
     */
    public function getCommand(): string
    {
        return $this->request['command'];
    }


    /**
     * Get request credentials
     * 
     * @return array Credentials
     */
    public function getCredentials(): array
    {
        return $this->request['credentials'] ?? array();
    }


    /**
     * Get id of a chat request refers to
     * 
     * @return string Chat id
     */
    public function getChatId(): string
    {
        return $this->request['chatId'] ?? '';
    }


    /**
     * Get request message
     * 
     * @return string Message
     */
    public function getMessage(): string
    {
        return $this->request['message'] ?? '';
    }


    /**
     * Get list of users to start private chat with 
     * 
     * @return array User logins
     */
    public function getPartners(): array
    {
        if (is_string($this->request['with'])) {
            return array($this->request['with']);
        }

        return array();
    }


    /**
     * Remember result of user authorization
     */
    public function setAuthResult(bool $authResult, string $login = '', string $authKey = ''): void
    {
        $response = $this->response[$this->connectionId];
        $response['userAuthorized'] = $authResult;

        if (!empty($login)) {
            $response['userName'] = $login;
        }
        
        if (!empty($authKey)) {
            $response['authKey'] = $authKey;
        }
        
        $this->response[$this->connectionId] = $response;
    }


    /**
     * Remember reason of authorization failure
     */
    public function setAuthReason(string $authReason): void
    {
        $this->response[$this->connectionId]['authReason'] = $authReason;
    }


    /**
     * Append messages for sending back to user(s)
     * 
     * @param Message $message Message object that should be transmitted to one or more chat clients
     * @param array $recipients Lisr of users to send $message to
     * @return void
     */
    public function append(Message $message, array $recipients): void
    {
        foreach ($recipients as $id) {
            if (!array_key_exists($id, $this->response) || !is_array($this->response[$id])) {
                $this->response[$id] = array();
            }

            switch ($message->getType()) {
                case 'user':
                    $bundle = 'users';
                    break;
                case 'room':
                    $bundle = 'chats';
                    break;
                case 'chat':
                    $bundle = 'messages';
                    break;     
                default:
                    return;   
            }

            if (!array_key_exists($bundle, $this->response[$id]) || 
                !is_array($this->response[$id][$bundle])) {
                $this->response[$id][$bundle] = array();
            }

            array_push($this->response[$id][$bundle], $message->getData());
        }
    }


    public function current()
    {
        return array_values($this->response)[$this->cursor];
    }


    public function key()
    {
        return array_keys($this->response)[$this->cursor];
    }


    public function next()
    {
        ++$this->cursor;
    }


    public function rewind()
    {
        $this->cursor = 0;
    }


    public function valid()
    {
        return count($this->response) > $this->cursor;
    }
}