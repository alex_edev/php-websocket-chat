<?php
namespace AlexEdev\Chat;

/**
 * Represents single connection to and its authorization data
 */
class Connection
{
    /** @var string Connection id */
    private $id;

    /** @var Database Chat object handling requests to the database */
    private $database;

    /** @var string Login of last successful authorization */
    private $login;

    public function __construct(Database $database, string $id)
    {
        $this->database = $database;
        $this->id = $id;
    }


    /**
     * Get connection id
     * 
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }


    /**
     * Get connection login
     * 
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login ?? '';
    }


    /**
     * Get connection status
     * 
     * @return string
     */
    public function getStatus(): string
    {
        if ($this->database->getActivityMarker($this->login.'_at_'.$this->getId())) {
            return 'active';
        } else {
            return 'away';
        }
    }


    /**
     * Apply authorization details to current connection
     * 
     * @param string $logins User login
     * @return void
     */
    public function authorize(string $login): void
    {
        $this->login = $login;
        $this->database->setActivityMarker($this->login.'_at_'.$this->getId());
    }


    /**
     * Remove authorization details from current connection
     * 
     * @return void
     */
    public function unauthorize(): void
    {
        if (isset($this->login)) {
            $this->database->deleteActivityMarker($this->login.'_at_'.$this->getId());
            unset($this->login);
        }
    }


    /**
     * Does this connection have authorization details applied
     * 
     * @return bool TRUE if there is user authorized by this connection, FALSE otherwise
     */
    public function isAuthorized(): bool
    {
        return !empty($this->login);
    }
}