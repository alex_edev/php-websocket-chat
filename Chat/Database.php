<?php
namespace AlexEdev\Chat;

use AlexEdev\Chat\Messages\ChatMessage;
use const Config\Chat\{
    MSG_READ_LIMIT,
    MSG_CACHE_LIMIT,
    USER_ACTIVE_TIMEOUT,
    EXPIRATION_TIME
};

/**
 * Database and cache handler
 */
class Database
{
    /** @var PDO Database object */
    private $db;

    /** @var Memcached Memcached client object */
    private $mc;

    /** @var PDOStatement Prepared statement for reading recent messages from specified chat history */
    private $readMsgSt;

    /** @var PDOStatement Prepared statement for reading members list for specified chat */
    private $readMbrSt;

    /** @var PDOStatement Prepared statement for inserting new message into chat history */
    private $insertMsgSt;

    /** @var PDOStatement Prepared statement for replacing a list of members of specified chat */
    private $replaceMbrSt;

    function __construct(\PDO $db, \Memcached $mc)
    {
        $this->db = $db;
        $this->mc = $mc;

        $this->readMsgSt = $this->db->prepare("SELECT FLOOR(UNIX_TIMESTAMP(`msg_timestamp`)*1000) as 
            `timestamp`, `message` FROM `chats_history` WHERE `chat_id`= ? ORDER BY `msg_timestamp` 
            DESC LIMIT ".MSG_READ_LIMIT);
        $this->readMbrSt = $this->db->prepare("SELECT `members` FROM `chats_members` WHERE `chat_id`= ?");
        $this->insertMsgSt = $this->db->prepare("INSERT INTO `chats_history` (`chat_id`, `message`, `origin`)
            VALUES (?, ?, ?)");
        $this->replaceMbrSt = $this->db->prepare("REPLACE INTO `chats_members` (`chat_id`, `members`)
            VALUES (?, ?)");
    }


    /**
     * Check if there is an error in commited operation
     *
     * @param int $code Memcached operation result code
     */
    private function checkMcResultCode(int $code): void
    {
        $in = debug_backtrace()[1]['function'];

        if ($code !== \Memcached::RES_SUCCESS && $code !== \Memcached::RES_NOTFOUND) {
            // Log details of a failed operation
            trigger_error("Memcached operation result code $code in $in()", E_USER_WARNING);
            // Stop handling request and set up error message for client
            throw new EdevChatException("Database operation error", 0);
        }  
    }


    /**
     * Get user activity marker
     * 
     * @return bool TRUE if marker found, FALSE otherwise
     */
    public function getActivityMarker(string $login): bool
    {
        return boolval($this->mc->get(preg_replace('/\s/', '', $login).'\active'));
    }


    /**
     * Set user activity marker
     * 
     * @param string $login User login
     */
    public function setActivityMarker(string $login): void
    {
        $this->mc->set(preg_replace('/\s/', '', $login).'\active', true, USER_ACTIVE_TIMEOUT);
        $this->checkMcResultCode($this->mc->getResultCode());
    }


    /**
     * Delete user activity marker
     * 
     * @param string $login User login
     */
    public function deleteActivityMarker(string $login): void
    {
        $this->mc->delete(preg_replace('/\s/', '', $login).'\active');
        $this->checkMcResultCode($this->mc->getResultCode());
    }


    /**
     * Get members of a specified chat
     * 
     * @param string $chatId Chat id
     * @return array List of members logins
     */
    public function getMembers(string $chatId): array
    {
        $members = $this->mc->get($chatId.'\members');

        if (!is_array($members)) { // if no members found
            // Try to find members in the database
            try {
                $this->readMbrSt->execute(array($chatId));
                $members = json_decode($this->readMbrSt->fetchColumn());

                if (!is_array($members)) {
                    // Log details of a failed operation
                    trigger_error("Expected to get array with members of chat '$chatId' from database in 
                        getMembers(), instead got ".print_r($members, true), E_USER_WARNING);
                    // Stop handling request and set up error message for client
                    throw new EdevChatException("Database operation error", 0);
                }

                // Cache found values
                $this->mc->set($chatId.'\members', $members, EXPIRATION_TIME);
                $this->checkMcResultCode($this->mc->getResultCode());
            } catch (\PDOException $e) {
                // Log details of a failed operation
                trigger_error($e->getMessage()." in ".$e->getFile()." on line ".$e->getLine(), E_USER_WARNING);
                // Stop handling request and exit the script
                exit;
            }
        }

        return $members;
    }


    /**
     * Set members of a specified chat
     * 
     * @param string $chatId Chat id
     * @param array $members List of members logins
     * @return void
     */
    public function setMembers(string $chatId, array $members): void
    {
        try {
            $this->replaceMbrSt->execute(array($chatId, json_encode($members, 256)));
        } catch (\PDOException $e) {
            // Log details of a failed operation
            trigger_error($e->getMessage()." in ".$e->getFile()." on line ".$e->getLine(), E_USER_WARNING);
            // Stop handling request and exit the script
            exit;
        }

        $this->mc->set($chatId.'\members', $members, EXPIRATION_TIME);
        $this->checkMcResultCode($this->mc->getResultCode());
    }


    /**
     * Get history of a specified chat
     * 
     * @param string $chatId Chat id
     * @return array List of messages (string timestamp => array message, ...)
     */
    public function getMessages(string $chatId): array
    {
        $chatHistory = $this->mc->get($chatId.'\history');

        if (!is_array($chatHistory)){
            $chatHistory = array();

            // Try to read messages from the database
            try {
                $this->readMsgSt->execute(array($chatId));

                foreach ($this->readMsgSt->fetchAll(\PDO::FETCH_ASSOC) as $value) {
                    $chatHistory[$value['timestamp']] = json_decode($value['message'], true);
                }

                ksort($chatHistory);
                $this->cacheMessages($chatHistory, $chatId);
            } catch (\PDOException $e) {
                // Log details of a failed operation
                trigger_error($e->getMessage()." in ".$e->getFile()." on line ".$e->getLine(), E_USER_WARNING);
                // Stop handling request and exit the script
                exit;
            }
        }

        return $chatHistory;
    }


    /**
     * Post message to the database 
     * 
     * @param ChatMessage $message Message object
     * @return void
     */
    public function postMessage(ChatMessage $message): void
    {
        if (!empty($message->getPostTimestamp())) {
            throw new EdevChatException("Forbidden to post message that has already been marked as posted", 0);
        }

        try {
            $this->db->beginTransaction();
            // Insert message to the database
            $this->insertMsgSt->execute(array($message->getChatId(), json_encode($message->getData(), 256), 
                $message->getOrigin()));
            // Get inserted message timestamp
            $res = $this->db->query('SELECT FLOOR(UNIX_TIMESTAMP(`msg_timestamp`)*1000) FROM `chats_history` 
                WHERE `id` = '.$this->db->lastInsertId());
            $this->db->commit();
            
            $timestamp = $res->fetchColumn();

            if (!is_numeric($timestamp) || intval($timestamp) != $timestamp) {
                // Log details of a failed operation
                trigger_error("Expected to get integer timestamp value in postMessage(), instead got "
                    .gettype($timestamp).' '.print_r($timestamp, true), E_USER_WARNING);
                // Stop handling request and set up error message for client
                throw new EdevChatException("Database operation error", 0);
            }

            $message->setPostTimestamp(intval($timestamp));
            $this->cacheMessages(array($timestamp => $message->getData()), $message->getChatId());
        } catch (\PDOException $e) {
            if ($this->db->inTransaction()) {
                $this->db->rollback();
            }
            
            // Log details of a failed operation
            trigger_error($e->getMessage()." in ".$e->getFile()." on line ".$e->getLine(), E_USER_WARNING);
            // Stop handling request and exit the script
            exit;
        }
    }


    /**
     * Cache a list of messages from specified chat in Memcached
     * 
     * @param array $messages List of messages to cache (timestamp => message)
     * @param string $chatId Chat id
     * @return void
     */
    private function cacheMessages(array $messages, string $chatId): void
    {
        $chatHistory = $this->mc->get($chatId.'\history');

        if (!is_array($chatHistory)){
            $chatHistory = array();
        }

        $chatHistory = $messages + $chatHistory; /* read http://php.net/manual/en/language.operators.array.php 
                                                    to get why this order of sum is used */

        if (($rest = count($chatHistory)-MSG_CACHE_LIMIT) > 0) {
            // Clean up old messages
            ksort($chatHistory);
            $chatHistory = array_slice($chatHistory, $rest, null, true);
        }
        
        $this->mc->set($chatId.'\history', $chatHistory, EXPIRATION_TIME);
        $this->checkMcResultCode($this->mc->getResultCode());
    }
}