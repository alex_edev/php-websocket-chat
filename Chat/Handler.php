<?php
namespace AlexEdev\Chat;

use AlexEdev\Chat\Rooms\RoomConstructor;
use AlexEdev\Chat\Messages\{
    RoomMessage,
    UserMessage,
    UserChatMessage
};

class EdevChatException extends \Exception {}

/**
 * This is the main chat class responsible for routing and handling requests
 */
class Handler
{
    /** @var PDO Database object */
    private $db;

    /** @var Memcached Memcached client object */
    private $mc;

    /** @var Database Is responsible for operations with database */
    private $database;

    /** @var Connections Maintains a list of client connections to chat server */
    private $connections;

    /** @var RoomConstructor Creates representations of chat rooms */
    private $roomConstructor;

    /** @var Callable User authorization callback */
    private $authCallback;

    public function __construct(\PDO $db, \Memcached $mc, Callable $authCallback)
    {
        $this->db = $db;
        $this->mc = $mc;
        $this->authCallback = $authCallback;
        $this->database = new Database($db, $mc);
        $this->connections = new Connections($this->database);
        $this->roomConstructor = new RoomConstructor($this->database);
    }


    /**
     * Route command to its handler method
     */
    public function handle(DataSet $dataSet): void
    {
        switch ($dataSet->getCommand()) {
            case 'signIn':
                $this->signIn($dataSet);
                break;
            case 'signOut':
                $this->signOut($dataSet);
                break;
            case 'disconnect':
                $this->closeConnection($dataSet);
                break;
            case 'getRecent':
                $this->getRecentMessages($dataSet);
                break;
            case 'getUsers':
                $this->getUsers($dataSet);
                break;
            case 'goPrivate':
                $this->goPrivateChat($dataSet);
                break;
            case 'postMessage':
                $this->postMessage($dataSet);
                break;
        }
    }


    /**
     * Authorize user and return operarion result
     */
    private function getAuthorization(DataSet $dataSet): bool
    {
        $auth = $this->authCallback->call($this, $dataSet->getCredentials());
        $dataSet->setAuthResult($auth['result'], $auth['login'] ?? '', $auth['key'] ?? '');
        $currentConnection = $this->getCurrentConnection($dataSet);

        if ($auth['result'] && !empty($auth['login'])) {
            $currentConnection->authorize($auth['login']);
        } else {
            $currentConnection->unauthorize();
            $dataSet->setAuthReason($auth['reason']);
        }

        return $auth['result'];
    }


    /**
     * Get current connection object
     */
    private function getCurrentConnection(DataSet $dataSet): Connection
    {
        return $this->connections->get($dataSet->getConnectionId());
    }


    /**
     * Broadcasts user status among all active client connections
     *
     * @param string $login User login
     * @param string $status User status
     */
    private function broadcastStatus(DataSet $dataSet, string $login, string $status): void
    {
        $message = new UserMessage($login, $status);
        $idList = $this->connections->getIdList();
        $dataSet->append($message, $idList);
    }


    /**
     * Sign in command handler
     */
    public function signIn(DataSet $dataSet): void
    {
        if ($this->getAuthorization($dataSet)) {
            $currentConnection = $this->getCurrentConnection($dataSet);
            $this->broadcastStatus($dataSet, $currentConnection->getLogin(), $currentConnection->getStatus()); 
        }
    }


    /**
     * Sign out command handler
     */
    public function signOut(DataSet $dataSet): void
    {
        $currentConnection = $this->getCurrentConnection($dataSet);
        $login = $currentConnection->getLogin();
        $currentConnection->unauthorize();
        $dataSet->setAuthResult(false);

        // Must be the last connection for this authorized user to broadcast offline status
        if (!empty($login) && !count($this->connections->filterByLogin($login))) {
            $this->broadcastStatus($dataSet, $login, 'offline'); 
        }
    }


    /**
     * Close connection command handler
     */
    public function closeConnection(DataSet $dataSet): void
    {
        $login = $this->getCurrentConnection($dataSet)->getLogin();
        $this->connections->close($dataSet->getConnectionId());

        // Must be the last connection for this authorized user to broadcast offline status
        if (!empty($login) && !count($this->connections->filterByLogin($login))) {
            $this->broadcastStatus($dataSet, $login, 'offline'); 
        }
    }


    /**
     * Go private chat with specified chat partner(s)
     */
    public function goPrivateChat(DataSet $dataSet): void
    {
        if (!$this->getAuthorization($dataSet)) {
            throw new EdevChatException("Cannot authorize to complete the action", 0);
        }
        
        $partners = $dataSet->getPartners();
        
        // Check that every partner exists
        foreach ($partners as $partner) {
            if (!count($this->connections->filterByLogin($partner))) {
                throw new EdevChatException("User $partner does not exist", 0);
            }
        }

        $login = $this->getCurrentConnection($dataSet)->getLogin();
        $members = array_merge($partners, array($login));
        $room = $this->roomConstructor->getByMembers($members);
        $message = new RoomMessage($room->getId(), $members);
        $curConnId = $dataSet->getConnectionId();
        $dataSet->append($message, array($curConnId));

        // Append chat room history if available
        foreach ($room->getMessages() as $message) {
            $dataSet->append($message, array($curConnId));
        }
    }


    /**
     * Get authorized users list and their activity statuses
     */
    public function getUsers(DataSet $dataSet): void
    {
        $curConnId = $dataSet->getConnectionId();
        
        foreach ($this->connections as $connection) {
            if ($connection->isAuthorized()) {
                $message = new UserMessage($connection->getLogin(), $connection->getStatus());
                $dataSet->append($message, array($curConnId));
            }
        }
    }


    /**
     * Get recent messages for specified chat
     */
    public function getRecentMessages(DataSet $dataSet):void
    {
        $this->getAuthorization($dataSet);
        $login = $this->getCurrentConnection($dataSet)->getLogin();
        $room = $this->roomConstructor->getById($dataSet->getChatId());
        
        if ($room->isChatMember($login)) {
            $curConnId = $dataSet->getConnectionId();

            foreach ($room->getMessages() as $message) {
                $dataSet->append($message, array($curConnId));
            }
        } else {
            throw new EdevChatException("$login has no rights to read chat '$chatId' history", 0);
        }
    }


    /**
     * Post message to the specified chat
     */
    public function postMessage(DataSet $dataSet): void
    {
        if (!$this->getAuthorization($dataSet)) {
            throw new EdevChatException("Cannot authorize to post message", 0);
        }

        $chatId = $dataSet->getChatId();
        $room = $this->roomConstructor->getById($chatId);
        $currentConnection = $this->getCurrentConnection($dataSet);
        $login = $currentConnection->getLogin();

        if ($room->isChatMember($login)) {
            $message = new UserChatMessage($login, $dataSet->getMessage(), $chatId);
            $this->database->postMessage($message);
            
            if ($room->getType() == 'main') {
                $idList = $this->connections->getIdList();
            } else {
                $members = $room->getMembers();
                $idList = array_keys($this->connections->filterMultiByLogins($members));
            }

            $dataSet->append($message, $idList);
            $this->broadcastStatus($dataSet, $currentConnection->getLogin(), $currentConnection->getStatus());
        } else {
            throw new EdevChatException("$login has no rights to post messages to chat '$chatId'", 0);
        }
    }
}