<?php
namespace AlexEdev\Chat\Messages;

/**
 * Message interface
 */
interface Message
{
    /**
     * Get message data representation for client side usage
     * 
     * @return array Message data package
     */
    public function getData(): array;


    /**
     * Get message type (user/room/chat)
     * 
     * @return string
     */
    public function getType(): string;
}