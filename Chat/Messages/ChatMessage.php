<?php
namespace AlexEdev\Chat\Messages;

/**
 * Chat message contains information about single chat post
 */
abstract class ChatMessage implements Message
{
    public function getType(): string
    {
        return 'chat';
    }


    /**
     * Get chat id message belongs to
     * 
     * @return string Chat id
     */
    abstract public function getChatId(): string;


    /**
     * Get message origin (usually login of a user who posted it or empty string for system messages)
     * 
     * @return string
     */
    abstract public function getOrigin(): string;


    /**
     * Get timestamp of posting message to chat
     * 
     * @return int Timestamp
     */
    abstract public function getPostTimestamp(): int;


    /**
     * Set timestamp of posting message to chat
     * 
     * @param int $timestamp
     * @return void
     */
    abstract public function setPostTimestamp(int $timestamp): void;
}