<?php
namespace AlexEdev\Chat\Messages;

/**
 * User message contains data about single authorized user in chat 
 */
class UserMessage implements Message
{
    /** @var string User login */
    private $login;

    /** @var array User status */
    private $status;

    public function __construct(string $login, string $status)
    {
        $this->login = $login;
        $this->status = $status;
    }


    public function getData(): array
    {
        return array(
            'login' => $this->login,
            'status' => $this->status
        );
    }


    public function getType(): string
    {
        return 'user';
    }
}