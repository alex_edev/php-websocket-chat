<?php
namespace AlexEdev\Chat\Messages;

/**
 * Room message contains data about single chat room
 */
class RoomMessage implements Message
{
    /** @var string Chat room id */
    private $id;

    /** @var array Chat room members logins */
    private $members;

    public function __construct(string $id, array $members)
    {
        $this->id = $id;
        $this->members = $members;
    }


    public function getData(): array
    {
        return array(
            'id' => $this->id,
            'members' => $this->members
        );
    }


    public function getType(): string
    {
        return 'room';
    }
}