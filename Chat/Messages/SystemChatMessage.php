<?php
namespace AlexEdev\Chat\Messages;

use AlexEdev\Chat\EdevChatException;

/**
 * Chat message posted by system
 */
class SystemChatMessage extends ChatMessage
{
    /** @var string Message header*/
    private $header;

    /** @var string Message content*/
    private $content;

    /** @var int Timestamp this message been posted to chat*/
    private $timestamp;

    public function __construct(string $header, string $chatId, int $timestamp = 0)
    {
        $this->header = $header;
        $this->chatId = $chatId;
        $this->timestamp = $timestamp;
    }


    public function getData(): array
    {
        $data = array(
            'format' => 'system',
            'header' => filter_var($this->header, FILTER_SANITIZE_FULL_SPECIAL_CHARS),
            'chatId' => filter_var($this->chatId, FILTER_SANITIZE_FULL_SPECIAL_CHARS),
        );

        if (!empty($this->timestamp)) {
            $data['timestamp'] = $this->timestamp;
        }

        return $data;
    }


    public function getChatId(): string
    {
        return $this->chatId;
    }


    public function getOrigin(): string
    {
        return '';
    }


    public function getPostTimestamp(): int
    {
        return $this->timestamp;
    }


    public function setPostTimestamp(int $timestamp): void
    {
        if (!empty($this->timestamp)) {
            throw new EdevChatException("Forbidden to modify not empty post timestamp", 0);
        }

        $this->timestamp = $timestamp;
    }
}