<?php
namespace AlexEdev\Chat;

/**
 * List of client connections to chat
 */
class Connections implements \Iterator
{
    /** @var Database Chat object handling requests to the database */
    private $database;

    /** @var array Array of connections where $key is a connection id, $value is a Chat\Connection object */
    private $connections = array();

    /** @var int Cursor for itetator realization */
    private $cursor = 0;

    public function __construct(Database $database)
    {
        $this->database = $database;
    }


    /**
     * Get connection object or create it if not exists
     * 
     * @return Connection Connection object
     */
    public function get(string $id): Connection
    {
        if (!isset($this->connections[$id])) {
            $this->connections[$id] = new Connection($this->database, $id);
        }

        return $this->connections[$id];
    }


    /**
     * Unauthorize and close specified chat connection (DOES NOT CLOSE TCP CONNECTION BETWEEN CLIENT AND 
     * WS SERVER)
     * 
     * @param string $id Id of a connection to close
     * @return void
     */
    public function close(string $id): void
    {
        if (isset($this->connections[$id])) {
            $this->connections[$id]->unauthorize();
            unset($this->connections[$id]);
        }  
    }


    /**
     * Filter chat connections by user login (there may be several connections authorized as the same user)
     * 
     * @param string $login User login
     * @return array List of connections
     */
    public function filterByLogin(string $login): array
    {
        $connections = array();

        foreach ($this->connections as $id => $connection) {
            if ($connection->getLogin() == $login) {
                $connections[$id] = $connection;
            }
        }

        return $connections;
    }


    /**
     * Filter chat connections by several login
     * 
     * @param array $logins User logins
     * @return array List of connections
     */
    public function filterMultiByLogins(array $logins): array
    {
        $connections = array();

        foreach ($logins as $value) {
            $connections += $this->filterByLogin($value);
        }

        return $connections;
    }


    /**
     * Get ids of all current connections to chat (both authorized and unauthorized)
     * 
     * @return array List of connections ids
     */
    public function getIdList(): array
    {
        return array_keys($this->connections);
    }


    public function current()
    {
        return array_values($this->connections)[$this->cursor];
    }


    public function key()
    {
        return array_keys($this->connections)[$this->cursor];
    }


    public function next()
    {
        ++$this->cursor;
    }


    public function rewind()
    {
        $this->cursor = 0;
    }


    public function valid()
    {
        return count($this->connections) > $this->cursor;
    }
}