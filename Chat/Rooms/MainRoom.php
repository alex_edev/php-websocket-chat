<?php
namespace AlexEdev\Chat\Rooms;

/**
 * Main chat room class 
 */
class MainRoom implements Room
{
    /** @var array Array of Message objects representing chat history */
    private $messages;

    public function __construct(array $messages)
    {
        $this->messages = $messages;
    }

    public function getId(): string
    {
        return '';
    }

    public function getType(): string
    {
        return 'main';
    }

    public function getMembers(): array
    {
        return array();
    }

    public function getMessages(): array
    {
        return $this->messages;
    }

    public function isChatMember(string $login): bool
    {
        return true;
    }
}