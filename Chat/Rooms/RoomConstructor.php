<?php
namespace AlexEdev\Chat\Rooms;

use AlexEdev\Chat\EdevChatException;
use AlexEdev\Chat\Database;
use AlexEdev\Chat\Messages\{
    UserChatMessage,
    SystemChatMessage
};

/**
 * 
 */
class RoomConstructor
{
    /** @var Database Chat object handling requests to the database */
    private $database;

    public function __construct(Database $database)
    {
        $this->database = $database;
    }


    /**
     * Get chat room by id
     * 
     * @param string $id Chat room id
     * @return Room Chat room object
     */
    public function getById(string $id): Room
    {
        $messages = $this->getChatMessages($id);

        if ($id === '') { // '' - main chat
            return new MainRoom($messages);
        }

        $members = $this->database->getMembers($id);

        return new PrivateRoom($id, $members, $messages);
    }


    /**
     * Get private chat room by its members list
     * 
     * @param array $members List of members logins
     * @return Room Chat room object
     */
    public function getByMembers(array $members): Room
    {
        $members = array_unique($members);

        if (count($members) < 2) {
            throw new EdevChatException("No chat rooms with less than 2 members allowed", 0);
        }

        sort($members);
        $id = hash('md5', implode($members));
        $this->database->setMembers($id, $members);
        $messages = $this->getChatMessages($id);

        return new PrivateRoom($id, $members, $messages);
    }


    /**
     * Get chat messages
     * 
     * @param string $id Chat room id
     * @return array Chat messages objects list
     */
    private function getChatMessages(string $id): array
    {
        $messages = array();

        foreach ($this->database->getMessages($id) as $timestamp => $message) {
            if ($message['format'] === 'chat' && is_string($message['header']) && 
                is_string($message['content']) && is_string($message['chatId'])) {
                array_push($messages, new UserChatMessage($message['header'], $message['content'], 
                    $message['chatId'], $timestamp));
            } else if ($message['format'] === 'system' && is_string($message['header']) && 
                is_string($message['chatId'])) {
                array_push($messages, new SystemChatMessage($message['header'], $message['chatId'], $timestamp));
            }
        }

        return $messages;
    }
}