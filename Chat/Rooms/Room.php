<?php
namespace AlexEdev\Chat\Rooms;

/**
 * Chat room interface
 */
interface Room
{
    /**
     * Get chat room id
     * 
     * @return string Chat id
     */
    public function getId(): string;


    /**
     * Get chat room type (private/main)
     * 
     * @return string Chat type
     */
    public function getType(): string;


    /**
     * Get chat room memers
     * 
     * @return array Members list
     */
    public function getMembers(): array;


    /**
     * Get chat room messages
     * 
     * @return array Messages objects list
     */
    public function getMessages(): array;


    /**
     * Is specified user a member of this chat
     * 
     * @return bool
     */
    public function isChatMember(string $login): bool;
}