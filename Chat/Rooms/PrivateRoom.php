<?php
namespace AlexEdev\Chat\Rooms;

/**
 * Private chat room class
 */
class PrivateRoom implements Room
{
    /** @var string Chat id */
    private $id;

    /** @var array Array of members of the chat */
    private $members;

    /** @var array Array of Message objects representing chat history */
    private $messages;

    public function __construct(string $id, array $members, array $messages)
    {
        $this->id = $id;
        $this->members = $members;
        $this->messages = $messages;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getType(): string
    {
        return 'private';
    }

    public function getMembers(): array
    {
        return $this->members;
    }

    public function getMessages(): array
    {
        return $this->messages;
    }

    public function isChatMember(string $login): bool
    {
        return in_array($login, $this->members);
    }
}